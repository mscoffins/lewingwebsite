<%-- 
    Document   : index
    Created on : 10-Aug-2015, 11:20:17
    Author     : Maco
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="beans.Picture"%>
<%@page import="Controllers.Database.DBAccess"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Lewing Homepage</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">

        <!-- Latest compiled and minified JavaScript -->
        <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
        <script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
        <link href="design.css" rel="stylesheet" type="text/css" />
    </head>
    <body >
        <%
            ArrayList<Picture> slidePics = DBAccess.getGalleryPics(1);
            String error = (String) request.getAttribute("error");
            String success = (String) request.getAttribute("success");
        %>
        <nav class="navbar navbar-default">
            <div class="container-fluid">  
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#mainNavBar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a href="#" class="navbar-brand"> Lewing homepage</a>
                </div>

                <div class="collapse navbar-collapse" id="mainNavBar">
                    <ul class="nav navbar-nav">
                        <li>
                            <form action="GuestLogin" method="POST">
                                <input type="submit" value="Guest Log In" class="btn btn-default"> 
                            </form> 
                        </li>
                        <li>
                            <button type="button" class="btn btn-default" data-toggle="modal" data-target="#RequestAccount">Request Account</button>
                        </li>
                        
                        <li>
                            <button type="button" class="btn btn-default" data-toggle="modal" data-target="#login">Log In</button>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <div class="modal fade" id="login">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h3>Log In</h3>
                    </div>
                    <form role="form" action="Login" method="POST">
                        <div class="modal-body">

                            <div class="form-group">
                                <input class="form-control" type="text" name="username" placeholder="username">
                            </div>
                            <div class="form-group">
                                <input class="form-control" type="password" name="password" placeholder="password">
                            </div>    


                        </div>
                        <div class="modal-footer">
                            <input type="submit" value="Log In" class="btn btn-block btn-success">
                        </div>
                    </form>
                </div>

            </div>
        </div>
        <div class="modal fade" id="RequestAccount">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h3>Request Account</h3>
                    </div>
                    <form role="form" action="RequestAccount" method="POST">
                        <div class="modal-body">

                            <div class="form-group">
                                <input class="form-control" type="text" name="name" placeholder="name">
                            </div>
                            <div class="form-group">
                                <input class="form-control" type="text" name="email" placeholder="email">
                            </div>    


                        </div>
                        <div class="modal-footer">
                            <input type="submit" value="Request Account" class="btn btn-block btn-success">
                        </div>
                    </form>
                </div>

            </div>
        </div>
        <div class="container-fluid index-bg">
            <div class="row">
                <% if(error != null){%>
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <%=error%>
                </div>
                <%}
                if(success!= null){%>
                <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <%=success%>
                </div>
                <%}%>
                <div class="col-md-offset-1 col-md-10">
                    <h1 class="center-text">Welcome to the Lewing family homepage</h1>
                    <div id="splashDisplay" class="carousel slide" data-ride="carousel">
                        <!-- Indicators -->
                        <ol class="carousel-indicators">
                            <%for(int i = 0; i < slidePics.size(); i++){
                                if(i == 0){%>
                                    <li data-target="#splashDisplay" data-slide-to="0" class="active"></li>
                                    <%}else{%>
                                    <li data-target="#splashDisplay" data-slide-to="<%=i%>"></li>
                            <%}}%>
                        </ol>

                        <!-- Wrapper for slides -->
                        <div class="carousel-inner" role="listbox">
                            <%for(int i = 0; i < slidePics.size(); i++ ){
                                Picture p = slidePics.get(i);
                                if(i == 0){%>
                            
                            <div class="item active">
                                <div class="col-md-offset-1">
                                <img src="Pics/<%=p.getLoc()%>" alt="<%=p.getDecription()%>" class="img-responsive">
                                </div>
                            </div>
                                <%}else{%>
                            <div class="item">
                                <div class="col-md-offset-1">
                                <img src="Pics/<%=p.getLoc()%>" alt="<%=p.getDecription()%>" class="img-responsive">
                                </div>
                            </div>

                           <%}}%>
                        </div>

                        <!-- Left and right controls -->
                        <a class="left carousel-control" href="#splashDisplay" role="button" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#splashDisplay" role="button" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div> 
            </div>
        </div>
    </body>
</html>
