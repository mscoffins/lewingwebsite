<%-- 
    Document   : ChangeAddress
    Created on : 17-Jun-2015, 13:51:31
    Author     : Maco
--%>

<%@page import="beans.Address"%>
<%@page import="java.util.ArrayList"%>
<%@page import="beans.UsefulLink"%>
<%@page import="Controllers.Database.DBAccess"%>
<%@page import="beans.User"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">

        <!-- Latest compiled and minified JavaScript -->
        <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
        <script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
        <link href="design.css" rel="stylesheet" type="text/css" />
        <title>Change Address</title>
    </head>
    <body>
        <%
            String username = (String) session.getAttribute("username");
            User user;
            if (!username.equals("guest")) {
                user = DBAccess.getUserByUsername(username);
            } else {
                user = new User("guest", "guest", "guesteamil", "guestimg");
                request.getRequestDispatcher("login.jsp").forward(request, response);
            }

            ArrayList<UsefulLink> links = DBAccess.getUsefulLinks();
            ArrayList<Address> addresses = DBAccess.getAddresses();
            String error = (String) request.getAttribute("error");
            String success = (String) request.getAttribute("success");
        %>

        <nav class="navbar navbar-default">
            <div class="container-fluid">  
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#mainNavBar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a href="home.jsp" class="navbar-brand"> Lewing homepage</a>
                </div>

                <div class="collapse navbar-collapse" id="mainNavBar">
                    <ul class="nav navbar-nav">
                        <li><a href="Galleries.jsp">Galleries</a></li>
                            <%if (DBAccess.validUser(username)) {%>
                        <li><a href="AddressBook.jsp">Address Book</a></li>  
                        <li><a href="ImageUpload.jsp">Image Upload</a></li>
                            <%}
                                if (DBAccess.validAdmin(username)) {%>
                        <li><a href="Admin.jsp">Admin</a></li>
                            <%}%>
                    </ul>
                    
                    <ul class="nav navbar-nav navbar-right">
                        <%if (DBAccess.validUser(username)) {%>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><%=user.getName()%><span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li>
                                    <form action="Logout" method="POST">
                                        <input type="submit" value="logout" class="col-md-offset-2 btn btn-default">
                                    </form>
                                </li>
                                <li role="separator" class="divider"></li>
                                <li><a href="ChangePassword.jsp">Change Password</a></li>
                                <li class="active"><a href="ChangeAddress.jsp">Change Address</a></li>
                            </ul>
                        </li>
                        <%} else {%>
                        <li>
                            <form action="Logout" method="POST">
                                <input type="submit" value="logout" class="col-md-offset-2 btn btn-default">
                            </form>
                        </li>
                        <%}%>
                    </ul>
                    
                </div>
            </div>
        </nav>
        <div class="container-fluid">

            <div class="row">
                <% if (error != null) {%>
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <%=error%>
                </div>
                <%}
                    if (success != null) {%>
                <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <%=success%>
                </div>
                <%}%>
                <div class="col-md-8"> 
                    <div class="col-md-offset-2 col-md-10">
                        <div class="input-form">
                            <form method="POST" action="ChangeAddress">
                                <select class="form-control" name="id">
                                    <%for (Address a : addresses) {%>
                                    <option value="<%=a.getId()%>"><%=a.toString()%></option>
                                    <%}%>
                                </select>
                                <input type="hidden" value="<%=username%>" name ="username">
                                <input type="submit" value="change address" class="btn btn-success">
                            </form>
                        </div>
                        <h1 class="center-text">-</h1>
                        <div class="input-form"> 
                            <form method="POST" action="CreateAddress">
                                <input class="form-control" type="text" name="nameNum" placeholder="house name/number">
                                <input class="form-control" type="text" name="street" placeholder="street">
                                <input class="form-control" type="text" name="town" placeholder="town">
                                <input class="form-control" type="text" name="city" placeholder="city">
                                <input class="form-control" type="text" name="postcode" placeholder="postcode">
                                <input type="hidden" value="<%=username%>" name ="username">
                                <input type="submit" value="change address" class="btn btn-success">
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-offset-1 col-md-3 link-scroll">
                    <div class="link-input">
                        <%if (DBAccess.validUser(username)) {%>

                        <h3 class="col-md-12">Post Link</h3>
                        <div id="toggleLink" class="collapse">
                            <form action="PostLink" method="POST" class="form-hotizontal">
                                <div class="form-group">
                                    <lable for="name" class="col-sm-3 control-label"><strong>Name</strong></lable>
                                    <div class="col-sm-9">
                                        <input class="form-control" type="text" id="name" name="name" placeholder="Name" maxlength="100">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <lable for="desc" class="col-sm-3 control-label"><strong>Description</strong></lable>
                                    <div class="col-sm-9">
                                        <input class="form-control" type="text" name="desc" placeholder="description" maxlength="300">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <lable for="url" class="col-sm-3 control-label"><strong>URL</strong></lable>
                                    <div class="col-sm-9">
                                        <input class=form-control" type ="text" name="url" placeholder="url" maxlength="100">
                                    </div>
                                </div>
                                <br>
                                <div class="form-group">
                                    <label for="private" class="col-sm-3 control-label">Privacy</label>
                                    <div class="col-sm-4">
                                        <div class="radio">
                                            <input type="radio" name="private" value="true" checked>Private
                                        </div>
                                    </div>
                                    <div class="col-sm-5">
                                        <div class="radio">
                                            <input type="radio" name="private" value="false">Public
                                        </div>
                                    </div>

                                </div>
                                <input type="hidden" name ="username" value="<%=username%>">
                                <div class="col-sm-offset-8">
                                    <input type="submit" value="post Link" class="btn btn-success">
                                </div>
                            </form>
                        </div>
                        <button href="#toggleLink" class="btn btn-info" data-toggle="collapse">toggle Link poster</button>
                        <%}%>
                    </div>

                    <%for (UsefulLink l : links) {
                            if (!l.isIsPrivate() || DBAccess.validUser(username)) {%>
                    <h3 class="center-text">-</h3>
                    <div class="link-disp">
                        <%if (!l.getUrl().startsWith("http://")) {%>
                        <h3><a href="http://<%=l.getUrl()%>" target="_blank"><%=l.getName()%></a></h3>
                            <%} else {%>
                        <h3><a href="<%=l.getUrl()%>" target="_blank"><%=l.getName()%></a></h3>
                            <%}%>
                        <h4><%=l.getDescription()%></h4>
                        <div class="post-footer">
                            <small><%=l.getPoster()%>
                                <br />
                                <%=l.getTime().substring(0, l.getTime().lastIndexOf('.'))%>, <%=l.getDate()%></small></div>
                                <%if (l.getPoster().equals(username) || DBAccess.validAdmin(username)) {%>
                        <div class="col-md-6">
                            <form action="DeleteLink" method="POST">
                                <input class="form-inline" type="hidden" value="<%=l.getId()%>" name="id">
                                <input type="submit" value="delete link" class="btn btn-danger">
                            </form>
                        </div>
                        <div class="col-md-6">
                            <form class="form-inline" action="GoToEditLink" method="POST">
                                <input type="hidden" value="<%=l.getId()%>" name="id">  
                                <input type="submit" value="edit link" class="btn btn-success">
                            </form>
                        </div>
                        <%}%>
                    </div>
                    <%}
                        }%>
                </div>
                <div class="col-md-12"><p><small>copyright Macauley Scoffins 2015</small></p></div>
            </div>
        </div>

    </body>
</html>
