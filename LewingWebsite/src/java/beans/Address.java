
package beans;

import Controllers.Database.DBAccess;
import java.sql.SQLException;


public class Address {
    private int id;

    
    private String nameNum;
    private String street;
    private String town;
    private String city;
    private String postcode;
    
    public Address(int id, String nameNum, String street, String town, String city, String postcode) {
        this.id = id;
        this.nameNum = nameNum;
        this.street = street;
        this.town = town;
        this.city = city;
        this.postcode = postcode;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNameNum() {
        return nameNum;
    }

    public void setNameNum(String nameNum) {
        this.nameNum = nameNum;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }
    
    public int persist() throws ClassNotFoundException, SQLException{
        return DBAccess.persistAddress(this);
    }
    
    @Override
    public String toString(){
        StringBuilder str = new StringBuilder();
        str.append(nameNum).append(" ").append(street).append(", ");
        str.append(town).append(", ").append(city).append(", ");
        str.append(postcode);
        return str.toString();
    }
}
