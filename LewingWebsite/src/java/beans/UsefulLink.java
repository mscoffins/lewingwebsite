package beans;

import Controllers.Database.DBAccess;
import java.sql.SQLException;


public class UsefulLink {
    private int id;
    private String name;
    private String description;
    private String url;
    private String poster;
    private String date;
    private String time;
    private boolean isPrivate;

    public UsefulLink(int id, String name, String description, String url, String poster, String date, String time, boolean isPrivate) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.url = url;
        this.poster = poster;
        this.date = date;
        this.time = time;
        this.isPrivate = isPrivate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public boolean isIsPrivate() {
        return isPrivate;
    }

    public void setIsPrivate(boolean isPrivate) {
        this.isPrivate = isPrivate;
    }

    public boolean persist() throws ClassNotFoundException, SQLException{
        return DBAccess.persistUsefulLinkPost(this);
    }
}
