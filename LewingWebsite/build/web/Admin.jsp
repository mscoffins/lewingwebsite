<%-- 
    Document   : home
    Created on : 09-Jun-2015, 16:14:47
    Author     : Maco
--%>


<%@page import="beans.UserRequest"%>
<%@page import="beans.UsefulLink"%>
<%@page import="java.util.ArrayList"%>
<%@page import="Controllers.Database.DBAccess"%>
<%@page import="beans.User"%>
<%@page import="beans.Picture"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">

        <!-- Latest compiled and minified JavaScript -->
        <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
        <script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
        <link href="design.css" rel="stylesheet" type="text/css" />
        <title>Admin</title>
    </head>
    <body>
        <%
            String username = (String) session.getAttribute("username");
            User user;
            if (DBAccess.validAdmin(username)) {
                user = DBAccess.getUserByUsername(username);
            } else {
                user = new User("guest", "guest", "guesteamil", "guestimg");
                request.getRequestDispatcher("login.jsp").forward(request, response);
            }
            ArrayList<UsefulLink> links = DBAccess.getUsefulLinks();
            ArrayList<UserRequest> urs = DBAccess.getUserRequests();
            ArrayList<User> users = DBAccess.getUsers();
            String error = (String) request.getAttribute("error");
            String success = (String) request.getAttribute("success");
        %>

        <nav class="navbar navbar-default">
            <div class="container-fluid">  
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#mainNavBar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a href="home.jsp" class="navbar-brand"> Lewing homepage</a>
                </div>

                <div class="collapse navbar-collapse" id="mainNavBar">
                    <ul class="nav navbar-nav">
                        <li><a href="Galleries.jsp">Galleries</a></li>
                            <%if (DBAccess.validUser(username)) {%>
                        <li><a href="AddressBook.jsp">Address Book</a></li>  
                        <li><a href="ImageUpload.jsp">Image Upload</a></li>
                            <%}
                                if (DBAccess.validAdmin(username)) {%>
                        <li class="active"><a href="Admin.jsp">Admin</a></li>
                            <%}%>
                    </ul>
                    
                    <ul class="nav navbar-nav navbar-right">
                        <%if (DBAccess.validUser(username)) {%>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><%=user.getName()%><span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li>
                                    <form action="Logout" method="POST">
                                        <input type="submit" value="logout" class="col-md-offset-2 btn btn-success">
                                    </form>
                                </li>
                                <li role="separator" class="divider"></li>
                                <li><a href="ChangePassword.jsp">Change Password</a></li>
                                <li><a href="ChangeAddress.jsp">Change Address</a></li>
                            </ul>
                        </li>
                        <%} else {%>
                        <li>
                            <form action="Logout" method="POST">
                                <input type="submit" value="logout" class="col-md-offset-2 btn btn-success">
                            </form>
                        </li>
                        <%}%>
                    </ul>
                    
                </div>
            </div>
        </nav>
        <div class="container-fluid">
            <div class="row">
                <% if (error != null) {%>
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <%=error%>
                </div>
                <%}
                    if (success != null) {%>
                <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <%=success%>
                </div>
                <%}%>
                <div class="col-md-8">
                    <div class ="col-md-offset-2 col-md-10">
                        <h1>User requests:</h1>
                        <%if (urs.size() == 0) {%>
                        <h3>No user requests</h3>
                        <h1 class="center-text">-</h1>
                        <%}%>

                        <%for (UserRequest ur : urs) {%>
                        <div class="col-md-12 data-disp">
                            <%=ur.getName()%>
                            <br />
                            <%=ur.getEmail()%>
                            <br />
                        </div>
                        <button type="button" class="btn btn-default" data-toggle="modal" data-target="#CreateUser<%=ur.getId()%>">Create User</button>
                        <div class="modal fade" id="CreateUser<%=ur.getId()%>">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h3>Create User</h3>
                                    </div>
                                    <form role="form" action="CreateUser" method="POST">
                                        <div class="modal-body">

                                            <div class="form-group">
                                                Username
                                                <input type="text" name="username" placeholder="username">
                                            </div>
                                            <div class="form-group">
                                                password
                                                <input type="password" name="password" placeholder="password">
                                            </div>    
                                            <input type="hidden" name="name" value="<%=ur.getName()%>">
                                            <input type="hidden" name="email" value="<%=ur.getEmail()%>">
                                            <input type="hidden" name="id" value="<%=ur.getId()%>">

                                        </div>
                                        <div class="modal-footer">
                                            <input type="submit" value="Create User" class="btn btn-block btn-success">
                                        </div>
                                    </form>
                                </div>

                            </div>
                        </div>
                        <h1 class="center-text">-</h1>
                        <%}%>
                        <div class="col-md-12 input-form">
                            <h1>Expire password</h1>

                            <form action="ExpirePassword" method="POST">
                                <select class="form-control" name="username">
                                    <%for (User u : users) {%>
                                    <option value="<%=u.getUsername()%>"><%=u.getName()%>, <%=u.getUsername()%></option>
                                    <%}%>  
                                </select>
                                <input type="submit" value="expire password" class="btn btn-success">
                            </form>
                        </div>
                        <h1 class="center-text">-</h1>
                        <div class="col-md-12 input-form">
                            <h1>remove user</h1>

                            <form action="RemoveUser" method="POST">
                                <select class="form-control" name="username">
                                    <%for (User u : users) {
                                        if (!u.getUsername().equals(username)) {%>
                                    <option value="<%=u.getUsername()%>"><%=u.getName()%>, <%=u.getUsername()%></option>
                                    <%}
                                    }%>  
                                </select>
                                <input type="submit" value="Remove User" class="btn btn-danger">
                            </form>
                        </div>

                    </div>

                </div>
                <div class="col-md-offset-1 col-md-3 link-scroll">
                    <div class="link-input">
                        <%if (DBAccess.validUser(username)) {%>

                        <h3 class="col-md-12">Post Link</h3>
                        <div id="toggleLink" class="collapse">
                            <form action="PostLink" method="POST" class="form-hotizontal">
                                <div class="form-group">
                                    <lable for="name" class="col-sm-3 control-label"><strong>Name</strong></lable>
                                    <div class="col-sm-9">
                                        <input class="form-control" type="text" id="name" name="name" placeholder="Name" maxlength="100">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <lable for="desc" class="col-sm-3 control-label"><strong>Description</strong></lable>
                                    <div class="col-sm-9">
                                        <input class="form-control" type="text" name="desc" placeholder="description" maxlength="300">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <lable for="url" class="col-sm-3 control-label"><strong>URL</strong></lable>
                                    <div class="col-sm-9">
                                        <input class=form-control" type ="text" name="url" placeholder="url" maxlength="100">
                                    </div>
                                </div>
                                <br>
                                <div class="form-group">
                                    <label for="private" class="col-sm-3 control-label">Privacy</label>
                                    <div class="col-sm-4">
                                        <div class="radio">
                                            <input type="radio" name="private" value="true" checked>Private
                                        </div>
                                    </div>
                                    <div class="col-sm-5">
                                        <div class="radio">
                                            <input type="radio" name="private" value="false">Public
                                        </div>
                                    </div>

                                </div>
                                <input type="hidden" name ="username" value="<%=username%>">
                                <div class="col-sm-offset-8">
                                    <input type="submit" value="post Link" class="btn btn-success">
                                </div>
                            </form>
                        </div>
                        <button href="#toggleLink" class="btn btn-info" data-toggle="collapse">toggle Link poster</button>
                        <%}%>
                    </div>

                    <%for (UsefulLink l : links) {
                            if (!l.isIsPrivate() || DBAccess.validUser(username)) {%>
                    <h3 class="center-text">-</h3>
                    <div class="link-disp">
                        <%if (!l.getUrl().startsWith("http://")) {%>
                        <h3><a href="http://<%=l.getUrl()%>" target="_blank"><%=l.getName()%></a></h3>
                            <%} else {%>
                        <h3><a href="<%=l.getUrl()%>" target="_blank"><%=l.getName()%></a></h3>
                            <%}%>
                        <h4><%=l.getDescription()%></h4>
                        <div class="post-footer">
                            <small><%=l.getPoster()%>
                                <br />
                                <%=l.getTime().substring(0, l.getTime().lastIndexOf('.'))%>, <%=l.getDate()%></small></div>
                                <%if (l.getPoster().equals(username) || DBAccess.validAdmin(username)) {%>
                            <form action="DeleteLink" method="POST">
                                <input class="form-inline" type="hidden" value="<%=l.getId()%>" name="id">
                                <input type="submit" value="delete link" class="btn btn-danger">
                            </form>
                            <form class="form-inline" action="GoToEditLink" method="POST">
                                <input type="hidden" value="<%=l.getId()%>" name="id">  
                                <input type="submit" value="edit link" class="btn btn-success">
                            </form>
                        <%}%>
                    </div>
                    <%}
                        }%>
                </div>
                <div class="col-md-12"><p><small>copyright Macauley Scoffins 2015</small></p></div>
            </div>
        </div>


    </body>
</html>
