/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers.Website;

import Controllers.Database.DBAccess;
import beans.Picture;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

/**
 *
 * @author Maco
 */
@WebServlet(name = "ImageUpload", urlPatterns = {"/ImageUpload"})
@MultipartConfig(fileSizeThreshold=1024*1024*2, // 2MB
             maxFileSize=1024*1024*10,      // 10MB
             maxRequestSize=1024*1024*50)
public class ImageUpload extends HttpServlet {
    private static final String SAVE_DIR="Users/Maco/Documents/NetBeansProjects/LewingWebsite/web/Pics";
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ClassNotFoundException, SQLException {
        response.setContentType("text/html;charset=UTF-8");
        try{
        String savePath = "C:" + File.separator + SAVE_DIR; //specify your path here
        File fileSaveDir=new File(savePath);
        if(!fileSaveDir.exists()){
            fileSaveDir.mkdir();
        }

        int gallId = Integer.parseInt(request.getParameter("gallery"));
        String desc = request.getParameter("desc");
        ArrayList<Boolean> success = new ArrayList<>();
        for (Part part : request.getParts()) {
            if(part.getContentType() != null){
                String fileName = extractFileName(part);
                int id = DBAccess.getNextPhotoID();
                fileName = id + fileName;
                File file = new File(fileSaveDir, fileName); // Or File.createTempFile("somefilename-", ".ext", uploads) if you want to autogenerate an unique name.

                try (InputStream input = part.getInputStream()) {  // How to obtain part is answered in http://stackoverflow.com/a/2424824
                    Files.copy(input, file.toPath());
                }
                //part.write(savePath + File.separator + fileName);
                
                Picture p = new Picture(0,fileName,desc,gallId);
                success.add(p.persist());

            }
        }
        boolean error = false;
        for(Boolean s:success){
            if(!s)
                error = true;
        }
        if(error)
            request.setAttribute("error", "Upload unsuccessful");
        else
            request.setAttribute("success", "image(s) uploaded");
        
        request.getRequestDispatcher("ImageUpload.jsp").forward(request, response);
        }catch(NumberFormatException | IOException | ServletException | ClassNotFoundException | SQLException e){
            System.out.println(e);
            request.setAttribute("error", "something went wrong");
            request.getRequestDispatcher("ImageUpload.jsp").forward(request, response);
        }
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ImageUpload.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(ImageUpload.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ImageUpload.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(ImageUpload.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private String extractFileName(Part part) {
        String contentDisp = part.getHeader("content-disposition");
        String[] items = contentDisp.split(";");
        for (String s : items) {
            if (s.trim().startsWith("filename")) {
                return s.substring(s.indexOf("=") + 2, s.length()-1);
            }
        }
        return "";
    }

}
