

<%-- 
    Document   : AddressBook
    Created on : 10-Jun-2015, 15:16:57
    Author     : Maco
--%>

<%@page import="beans.Address"%>
<%@page import="java.util.ArrayList"%>
<%@page import="beans.UsefulLink"%>
<%@page import="Controllers.Database.DBAccess"%>
<%@page import="beans.User"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">

        <!-- Latest compiled and minified JavaScript -->
        <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
        <script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
        <link href="design.css" rel="stylesheet" type="text/css" />
        <title>address book</title>

        <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script>
        <script type="text/javascript">

            function getPosition(callback) {
                var geocoder = new google.maps.Geocoder();
                var postcode = document.getElementById("postcode").value;

                geocoder.geocode({'address': postcode}, function (results, status)
                {
                    if (status == google.maps.GeocoderStatus.OK)
                    {
                        callback({
                            latt: results[0].geometry.location.lat(),
                            long: results[0].geometry.location.lng()
                        });
                    }
                });
            }

            function setup_map(latitude, longitude) {
                var _position = {lat: latitude, lng: longitude};

                var mapOptions = {
                    zoom: 16,
                    center: _position
                }

                var map = new google.maps.Map(document.getElementById('map'), mapOptions);

                var marker = new google.maps.Marker({
                    position: mapOptions.center,
                    map: map
                });
            }

            window.onload = function () {
                setup_map(51.5073509, -0.12775829999998223);

                document.getElementById("form").onsubmit = function () {
                    getPosition(function (position) {

                        var text = document.getElementById("text")
                        text.innerHTML = "Marker position: { Longitude: " + position.long + ",  Latitude:" + position.latt + " }";

                        setup_map(position.latt, position.long);
                    });
                }
            }
        </script>  
    </head>
    <body>
        <%
            String username = (String) session.getAttribute("username");
            User user;
            if (!username.equals("guest")) {
                user = DBAccess.getUserByUsername(username);
            } else {
                user = new User("guest", "guest", "guesteamil", "guestimg");
            }

            ArrayList<UsefulLink> links = DBAccess.getUsefulLinks();
            ArrayList<Address> addresses = DBAccess.getAddresses();
            String error = (String) request.getAttribute("error");
            String success = (String) request.getAttribute("success");
        %>

        <nav class="navbar navbar-default">
            <div class="container-fluid">  
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#mainNavBar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a href="home.jsp" class="navbar-brand"> Lewing homepage</a>
                </div>

                <div class="collapse navbar-collapse" id="mainNavBar">
                    <ul class="nav navbar-nav">
                        <li><a href="Galleries.jsp">Galleries</a></li>
                            <%if (DBAccess.validUser(username)) {%>
                        <li class="active"><a href="AddressBook.jsp">Address Book</a></li>  
                        <li><a href="ImageUpload.jsp">Image Upload</a></li>
                            <%}
                                if (DBAccess.validAdmin(username)) {%>
                        <li><a href="Admin.jsp">Admin</a></li>
                            <%}%>
                    </ul>
                    
                    <ul class="nav navbar-nav navbar-right">
                        <%if (DBAccess.validUser(username)) {%>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><%=user.getName()%><span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li>
                                    <form action="Logout" method="POST">
                                        <input type="submit" value="logout" class="col-md-offset-2 btn btn-success">
                                    </form>
                                </li>
                                <li role="separator" class="divider"></li>
                                <li><a href="ChangePassword.jsp">Change Password</a></li>
                                <li><a href="ChangeAddress.jsp">Change Address</a></li>
                            </ul>
                        </li>
                        <%} else {%>
                        <li>
                            <form action="Logout" method="POST">
                                <input type="submit" value="logout" class="col-md-offset-2 btn btn-success">
                            </form>
                        </li>
                        <%}%>
                    </ul>
                    
                </div>
            </div>
        </nav>

        <div class="container-fluid">
            <div class="row">
                <% if(error != null){%>
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <%=error%>
                </div>
                <%}
                if(success!= null){%>
                <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <%=success%>
                </div>
                <%}%>
                <div class="col-md-8">
                    <div class="col-md-offset-1 col-md-11 input-form">
                        <form action="javascript:void(0)" id="form">
                            <select class="form-control" id="postcode">
                                <%for (Address a : addresses) {%>
                                <option value="<%=a.getPostcode()%>"><%=a.toString()%></option> 
                                <%}%>
                            </select>
                            <input type="submit" value="show address" class="btn btn-success">
                        </form>
                        <div id="map" class="map"></div> 
                        <div id="text" class="map-text"></div>

                    </div>
                    <%for (Address a : addresses) {%>
                    <div class="col-md-12"><h1 class="center-text">-</h1></div>
                    <div class="col-md-offset-1 col-md-7 data-disp">
                        <address>
                            <%=a.getNameNum()%>
                            <br />
                            <%=a.getStreet()%>
                            <br />
                            <%=a.getTown()%>
                            <br />
                            <%=a.getCity()%>
                            <br />
                            <%=a.getPostcode()%>
                        </address>
                    </div>
                    <div class="col-md-4 data-disp">
                        <address>
                            <strong>at this address:</strong><br />
                            <% ArrayList<String> names = DBAccess.getUsersAtAddress(a.getId());
                            for (String n : names) {%>

                            <%=n%>
                            <br />
                            <%}%>
                        </address>
                    </div>

                    <%}%>
                </div>

                <div class="col-md-offset-1 col-md-3 link-scroll">
                    <div class="link-input">
                        <%if (DBAccess.validUser(username)) {%>

                        <h3 class="col-md-12">Post Link</h3>
                        <div id="toggleLink" class="collapse">
                            <form action="PostLink" method="POST" class="form-hotizontal">
                                <div class="form-group">
                                    <lable for="name" class="col-sm-3 control-label"><strong>Name</strong></lable>
                                    <div class="col-sm-9">
                                        <input class="form-control" type="text" id="name" name="name" placeholder="Name" maxlength="100">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <lable for="desc" class="col-sm-3 control-label"><strong>Description</strong></lable>
                                    <div class="col-sm-9">
                                        <input class="form-control" type="text" name="desc" placeholder="description" maxlength="300">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <lable for="url" class="col-sm-3 control-label"><strong>URL</strong></lable>
                                    <div class="col-sm-9">
                                        <input class=form-control" type ="text" name="url" placeholder="url" maxlength="100">
                                    </div>
                                </div>
                                <br>
                                <div class="form-group">
                                    <label for="private" class="col-sm-3 control-label">Privacy</label>
                                    <div class="col-sm-4">
                                        <div class="radio">
                                            <input type="radio" name="private" value="true" checked>Private
                                        </div>
                                    </div>
                                    <div class="col-sm-5">
                                        <div class="radio">
                                            <input type="radio" name="private" value="false">Public
                                        </div>
                                    </div>

                                </div>
                                <input type="hidden" name ="username" value="<%=username%>">
                                <div class="col-sm-offset-8">
                                    <input type="submit" value="post Link" class="btn btn-success">
                                </div>
                            </form>
                        </div>
                        <button href="#toggleLink" class="btn btn-info" data-toggle="collapse">toggle Link poster</button>
                        <%}%>
                    </div>

                    <%for (UsefulLink l : links) {
                            if (!l.isIsPrivate() || DBAccess.validUser(username)) {%>
                    <h3 class="center-text">-</h3>
                    <div class="link-disp">
                        <%if(!l.getUrl().startsWith("http://")){%>
                        <h3><a href="http://<%=l.getUrl()%>" target="_blank"><%=l.getName()%></a></h3>
                            <%}else{%>
                        <h3><a href="<%=l.getUrl()%>" target="_blank"><%=l.getName()%></a></h3>
                            <%}%>
                        <h4><%=l.getDescription()%></h4>
                        <div class="post-footer">
                            <small><%=l.getPoster()%>
                                <br />
                                <%=l.getTime().substring(0, l.getTime().lastIndexOf('.'))%>, <%=l.getDate()%></small></div>
                                <%if (l.getPoster().equals(username)) {%>
                            <form action="DeleteLink" method="POST">
                                <input class="form-inline" type="hidden" value="<%=l.getId()%>" name="id">
                                <input type="submit" value="delete link" class="btn btn-danger">
                            </form>
                            <form class="form-inline" action="GoToEditLink" method="POST">
                                <input type="hidden" value="<%=l.getId()%>" name="id">  
                                <input type="submit" value="edit link" class="btn btn-success">
                            </form>
                        <%}%>
                    </div>
                    <%}
                        }%>
                </div>
            </div>
        </div>

    </body>
</html>
