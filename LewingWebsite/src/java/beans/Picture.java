package beans;

import Controllers.Database.DBAccess;
import java.sql.SQLException;


public class Picture {
    private int id;
    private String loc;
    private String decription;
    private int galleryID;

    public Picture(int id, String loc, String decription, int galleryID) {
        this.id = id;
        this.loc = loc;
        this.decription = decription;
        this.galleryID = galleryID;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLoc() {
        return loc;
    }

    public void setLoc(String loc) {
        this.loc = loc;
    }

    public String getDecription() {
        return decription;
    }

    public void setDecription(String decription) {
        this.decription = decription;
    }

    public int getGalleryID() {
        return galleryID;
    }

    public void setGalleryID(int galleryID) {
        this.galleryID = galleryID;
    }
    
    public String toString(){
        return this.loc + ", " + this.decription;
    }
    
    public boolean persist() throws ClassNotFoundException, SQLException{
        return DBAccess.persistImage(this);
    }
}
