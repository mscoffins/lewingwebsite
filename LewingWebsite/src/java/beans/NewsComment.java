/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import Controllers.Database.DBAccess;
import java.sql.SQLException;

/**
 *
 * @author Maco
 */
public class NewsComment {
    private int id;
    private String poster;
    private int newsId;
    private String comment;
    private String date;
    private String Time;

    public NewsComment(int id, String poster, int newsId, String comment, String date, String Time) {
        this.id = id;
        this.poster = poster;
        this.newsId = newsId;
        this.comment = comment;
        this.date = date;
        this.Time = Time;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    public int getNewsId() {
        return newsId;
    }

    public void setNewsId(int newsId) {
        this.newsId = newsId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return Time;
    }

    public void setTime(String Time) {
        this.Time = Time;
    }
    
    public boolean persist() throws ClassNotFoundException, SQLException{
        return DBAccess.persistComment(this);
    }
}
