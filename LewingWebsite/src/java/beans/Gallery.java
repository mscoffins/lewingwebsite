
package beans;

import Controllers.Database.DBAccess;
import java.sql.SQLException;


public class Gallery {
    private int id;
    private String name;
    private String description;
    private String thumbNail;
    private boolean isPrivate;
    private String poster;

    public Gallery(int id, String name, String description, String thumbNail, boolean isPrivate, String poster) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.thumbNail = thumbNail;
        this.isPrivate = isPrivate;
        this.poster = poster;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getThumbNail() {
        return thumbNail;
    }

    public void setThumbNail(String thumbNail) {
        this.thumbNail = thumbNail;
    }

    public boolean getIsPrivate() {
        return isPrivate;
    }

    public void setIsPrivate(boolean isPrivate) {
        this.isPrivate = isPrivate;
    }

    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }
   
    public boolean persist() throws ClassNotFoundException, SQLException{
        return DBAccess.persistGallery(this);
    }
}
