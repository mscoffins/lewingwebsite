﻿CREATE TABLE users
(
  name character varying(60),
  username character varying(60) NOT NULL,
  email character varying(100),
  password character varying(64),
  profileimg character varying(250),
  pwexpire timestamp without time zone,
  CONSTRAINT users_pkey PRIMARY KEY (username)
);

CREATE TABLE admins
(
  id serial NOT NULL,
  username character varying(60),
  CONSTRAINT admins_pkey PRIMARY KEY (id),
  CONSTRAINT admins_username_fkey FOREIGN KEY (username)
      REFERENCES users (username) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE CASCADE
);

CREATE TABLE galleries
(
  id serial NOT NULL,
  name character varying(100),
  description character varying(256),
  thumbnail character varying(128),
  private boolean,
  poster character varying(60),
  CONSTRAINT galleries_pkey PRIMARY KEY (id),
  CONSTRAINT galleries_poster_fkey FOREIGN KEY (poster)
      REFERENCES users (username) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE CASCADE
);

CREATE TABLE address
(
  id serial NOT NULL,
  namenum character varying(60),
  street character varying(60),
  town character varying(60),
  city character varying(60),
  postcode character varying(8),
  CONSTRAINT address_pkey PRIMARY KEY (id)
);

CREATE TABLE news
(
  id serial NOT NULL,
  title character varying(100),
  content character varying(2048),
  poster character varying(60),
  date date,
  "time" time without time zone,
  private boolean,
  image character varying(50),
  CONSTRAINT news_pkey PRIMARY KEY (id),
  CONSTRAINT news_poster_fkey FOREIGN KEY (poster)
      REFERENCES users (username) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE CASCADE
);

CREATE TABLE newscomment
(
  id serial NOT NULL,
  poster character varying(60),
  newsid integer,
  comment character varying(1024),
  date date,
  "time" time without time zone,
  CONSTRAINT newscomment_pkey PRIMARY KEY (id),
  CONSTRAINT newscomment_newsid_fkey FOREIGN KEY (newsid)
      REFERENCES news (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE CASCADE,
  CONSTRAINT newscomment_poster_fkey FOREIGN KEY (poster)
      REFERENCES users (username) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE CASCADE
);

CREATE TABLE photos
(
  id serial NOT NULL,
  description character varying(128),
  galleryid integer,
  loc character varying(256),
  CONSTRAINT photos_pkey PRIMARY KEY (id),
  CONSTRAINT photos_galleryid_fkey FOREIGN KEY (galleryid)
      REFERENCES galleries (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE CASCADE
);

CREATE TABLE usefullinks
(
  id serial NOT NULL,
  name character varying(100),
  description character varying(300),
  url character varying(100),
  poster character varying(60),
  date date,
  "time" time without time zone,
  private boolean,
  CONSTRAINT usefullinks_pkey PRIMARY KEY (id),
  CONSTRAINT usefullinks_poster_fkey FOREIGN KEY (poster)
      REFERENCES users (username) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE CASCADE
);

CREATE TABLE useraddress
(
  id serial NOT NULL,
  username character varying(60),
  addressid integer,
  CONSTRAINT useraddress_pkey PRIMARY KEY (id),
  CONSTRAINT useraddress_addressid_fkey FOREIGN KEY (addressid)
      REFERENCES address (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE CASCADE,
  CONSTRAINT useraddress_username_fkey FOREIGN KEY (username)
      REFERENCES users (username) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE CASCADE
);

CREATE TABLE userrequests
(
  id serial NOT NULL,
  name character varying(60),
  email character varying(60),
  CONSTRAINT userrequests_pkey PRIMARY KEY (id)
);
