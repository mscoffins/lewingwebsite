
package beans;

import Controllers.Database.DBAccess;
import java.sql.SQLException;

public class News {
    private int id;
    private String title;
    private String content;
    private String poster;
    private String date;
    private String time;
    private boolean isPrivate;
    private String image;

    public News(int id, String title, String content, String poster, String date, String time, boolean isPrivate, String img) {
        this.id = id;
        this.title = title;
        this.content = content;
        this.poster = poster;
        this.date = date;
        this.time = time;
        this.isPrivate = isPrivate;
        this.image = img;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public boolean isIsPrivate() {
        return isPrivate;
    }

    public void setIsPrivate(boolean isPrivate) {
        this.isPrivate = isPrivate;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public boolean persist() throws ClassNotFoundException, SQLException{
        return DBAccess.persistNewsPost(this);
    }
}
