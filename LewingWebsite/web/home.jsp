<%-- 
    Document   : home
    Created on : 09-Jun-2015, 16:14:47
    Author     : Maco
--%>


<%@page import="beans.UsefulLink"%>
<%@page import="java.util.ArrayList"%>
<%@page import="beans.News"%>
<%@page import="Controllers.Database.DBAccess"%>
<%@page import="beans.User"%>
<%@page import="beans.Picture"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">

        <!-- Latest compiled and minified JavaScript -->
        <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
        <script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
        <link href="design.css" rel="stylesheet" type="text/css" />
        <title>home</title>
    </head>
    <body>

        <%
            String username = (String) session.getAttribute("username");
            User user;
            if (DBAccess.validUser(username)) {
                user = DBAccess.getUserByUsername(username);
            } else {
                user = new User("guest", "guest", "guesteamil", "guestimg");
            }
            ArrayList<News> news = DBAccess.getNews();
            ArrayList<UsefulLink> links = DBAccess.getUsefulLinks();
            int start = 0;
            if (request.getParameter("start") != null) {
                start = Integer.parseInt(request.getParameter("start"));
            }
            int end = start + 10;
            if (end > news.size()) {
                end = news.size();
            }
            String error = (String) request.getAttribute("error");
            String success = (String) request.getAttribute("success");

        %>
        <nav class="navbar navbar-default">
            <div class="container-fluid">  
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#mainNavBar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a href="home.jsp" class="navbar-brand"> Lewing homepage</a>
                </div>

                <div class="collapse navbar-collapse" id="mainNavBar">
                    <ul class="nav navbar-nav">
                        <li><a href="Galleries.jsp">Galleries</a></li>
                            <%if (DBAccess.validUser(username)) {%>
                        <li><a href="AddressBook.jsp">Address Book</a></li>  
                        <li><a href="ImageUpload.jsp">Image Upload</a></li>
                            <%}
                                if (DBAccess.validAdmin(username)) {%>
                        <li><a href="Admin.jsp">Admin</a></li>
                            <%}%>
                    </ul>

                    <ul class="nav navbar-nav navbar-right">
                        <%if (DBAccess.validUser(username)) {%>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><%=user.getName()%><span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li>
                                    <form action="Logout" method="POST">
                                        <input type="submit" value="logout" class="col-md-offset-2 btn btn-default">
                                    </form>
                                </li>
                                <li role="separator" class="divider"></li>
                                <li><a href="ChangePassword.jsp">Change Password</a></li>
                                <li><a href="ChangeAddress.jsp">Change Address</a></li>
                            </ul>
                        </li>
                        <%} else {%>
                        <li>
                            <form action="Logout" method="POST">
                                <input type="submit" value="logout" class="col-md-offset-2 btn btn-default">
                            </form>
                        </li>
                        <%}%>
                    </ul>

                </div>
            </div>
        </nav>
        <div class="container-fluid">

            <div class="row">
                <% if (error != null) {%>
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <%=error%>
                </div>
                <%}
                    if (success != null) {%>
                <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <%=success%>
                </div>
                <%}%>
                <div class="col-md-8" >

                    <div class="col-md-offset-2 col-md-10 input-form">

                        <%if (DBAccess.validUser(username)) {
                                ArrayList<Picture> pics = DBAccess.getPictures();%>

                        <h3 class="col-md-12">Post news item</h3>
                        <div id="toggleNews" class="collapse">
                            <form action="PostNews" method="POST" class="form-hotizontal">
                                <div class="form-group">
                                    <lable for="title" class="col-sm-3 control-label"><strong>Title</strong></lable>
                                    <div class="col-sm-9">
                                        <input class="form-control" type="text" id="title" name="title" placeholder="Title" maxlength="100">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="content" class="col-sm-3 control-label">Content</label>
                                    <div class="col-sm-9">
                                        <textarea class="form-control" rows="3" maxlength="2048" name="content" placeholder="content"></textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="private" class="col-sm-3 control-label">Privacy</label>
                                    <div class="col-sm-4">
                                        <div class="radio">
                                            <input type="radio" name="private" value="true" checked>Private
                                        </div>
                                    </div>
                                    <div class="col-sm-5">
                                        <div class="radio">
                                            <input type="radio" name="private" value="false">Public
                                        </div>
                                    </div>

                                </div>

                                <input type="hidden" name ="username" value="<%=username%>">
                                <div class="form-group">
                                    <label for="image" class="col-sm-3 control-label">Image</label>
                                    <div class="col-sm-9">
                                        <select class="form-control" name="image">
                                            <option value="no image">none</option>
                                            <%for (Picture p : pics) {%>
                                            <option value="<%=p.getLoc()%>"><%=p.toString()%></option>
                                            <%}%>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <br>
                                    <input type="submit" value="post news" class="btn btn-block btn-success">
                                </div>
                                        <br>
                            </form>
                                        
                        </div>
                                        
                        <button href="#toggleNews" class="btn btn-info" data-toggle="collapse">toggle news poster</button>
                        <%}%>
                    </div>
                    <%for (int i = start; i < end; i++) {
                            News n = news.get(i);
                            if (!n.isIsPrivate() || DBAccess.validUser(username)) {%>
                    <h1 class="center-text">-</h1>
                    <div class="col-md-12 data-disp">
                        <%if (!n.getImage().equals("no image")) {%>
                        <div class="col-md-6" >
                            <img class="img-responsive img-rounded" alt="<%=n.getTitle()%>" src="Pics/<%=n.getImage()%>">
                        </div>
                        <div class="col-md-6">    
                            <h2><strong><%=n.getTitle()%></strong></h2>
                            <h3><%=n.getContent()%></h3>
                            <div class="post-footer">
                                <small><%=n.getPoster()%><br>
                                    <%=n.getTime().substring(0, n.getTime().lastIndexOf('.'))%>, <%=n.getDate()%></small>
                            </div>
                                    <div class="col-md-12">
                                        <form class="form-hotizontal" action="viewNewsComments.jsp" method="POST">
                                            <input type="hidden" value="<%=n.getId()%>" name="newsId">
                                            <input type="submit" value="Comments" class="btn btn-block btn-info">
                                        </form>
                                            <br>
                                    </div>
                                    <%if (n.getPoster().equals(username)) {%>
                            <div class="col-md-6">
                                <form action="DeleteNews" method="POST">
                                    <input type="hidden" value="<%=n.getId()%>" name="id">
                                    <input type="submit" value="delete post" class="btn btn-danger">
                                </form>
                            </div>
                            <div class="col-md-6">
                                <form action="GoToEditNews" method="POST">
                                    <input type="hidden" value="<%=n.getId()%>" name="id">
                                    <input type="submit" value="edit post" class="btn btn-success">
                                </form>
                            </div>
                            <%}%>
                        </div>
                        <%} else {%>
                        <div class="col-md-12">    
                            <h2><strong><%=n.getTitle()%></strong></h2>
                            <h3><%=n.getContent()%></h3>
                            <div class="post-footer">
                                <small><%=n.getPoster()%><br>
                                    <%=n.getTime().substring(0, n.getTime().lastIndexOf('.'))%>, <%=n.getDate()%></small></div>
                                    <div class="col-md-12">
                                        <form class="form-hotizontal" action="viewNewsComments.jsp" method="POST">
                                            <input type="hidden" value="<%=n.getId()%>" name="newsId">
                                            <input type="submit" value="Comments" class="btn btn-block btn-info">
                                        </form>
                                            <br>
                                    </div>
                                    <%if (n.getPoster().equals(username) || DBAccess.validAdmin(username)) {%>
                            <div class="col-md-6">
                                <form action="DeleteNews" method="POST">
                                    <input type="hidden" value="<%=n.getId()%>" name="id">
                                    <input type="submit" value="delete post" class="btn btn-danger">
                                </form>
                            </div>
                            <div class="col-md-6">
                                <form action="GoToEditNews" method="POST">
                                    <input type="hidden" value="<%=n.getId()%>" name="id">
                                    <input type="submit" value="edit post" class="btn btn-success">
                                </form>
                            </div>
                            <%}%>
                        </div>
                        <%}%>
                    </div>

                    <%}
                        }%>
                    <div class="col-md-offset-5 col-md-1">
                        <%if (start != 0) {
                                int newStart = start - 10;
                        %>
                        <form action="home.jsp" method="GET">
                            <input type="hidden" value="<%=newStart%>" name="start">
                            <input type="submit" value="prev page" class="btn btn-info">
                        </form>
                        <%}%>
                    </div>
                    <div class="col-md-6">
                        <%if (end != news.size()) {
                                int newStart = start + 10;
                        %>
                        <form action="home.jsp" method="GET">
                            <input type="hidden" value="<%=newStart%>" name="start">
                            <input type="submit" value="next page" class="btn btn-info">
                        </form>
                        <%}%>
                    </div>
                    <br>

                </div>
                <div class="col-md-offset-1 col-md-3 link-scroll">
                    <div class="link-input">
                        <%if (DBAccess.validUser(username)) {%>

                        <h3 class="col-md-12">Post Link</h3>
                        <div id="toggleLink" class="collapse">
                            <form action="PostLink" method="POST" class="form-hotizontal">
                                <div class="form-group">
                                    <lable for="name" class="col-sm-3 control-label"><strong>Name</strong></lable>
                                    <div class="col-sm-9">
                                        <input class="form-control" type="text" id="name" name="name" placeholder="Name" maxlength="100">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <lable for="desc" class="col-sm-3 control-label"><strong>Description</strong></lable>
                                    <div class="col-sm-9">
                                        <input class="form-control" type="text" name="desc" placeholder="description" maxlength="300">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <lable for="url" class="col-sm-3 control-label"><strong>URL</strong></lable>
                                    <div class="col-sm-9">
                                        <input class=form-control" type ="text" name="url" placeholder="url" maxlength="100">
                                    </div>
                                </div>
                                <br>
                                <div class="form-group">
                                    <label for="private" class="col-sm-3 control-label">Privacy</label>
                                    <div class="col-sm-4">
                                        <div class="radio">
                                            <input type="radio" name="private" value="true" checked>Private
                                        </div>
                                    </div>
                                    <div class="col-sm-5">
                                        <div class="radio">
                                            <input type="radio" name="private" value="false">Public
                                        </div>
                                    </div>

                                </div>
                                <input type="hidden" name ="username" value="<%=username%>">
                                <div class="col-sm-offset-8">
                                    <input type="submit" value="post Link" class="btn btn-success">
                                </div>
                            </form>
                        </div>
                        <button href="#toggleLink" class="btn btn-info" data-toggle="collapse">toggle Link poster</button>
                        <%}%>
                    </div>

                    <%for (UsefulLink l : links) {
                            if (!l.isIsPrivate() || DBAccess.validUser(username)) {%>
                    <h3 class="center-text">-</h3>
                    <div class="link-disp">
                        <%if (!l.getUrl().startsWith("http://")) {%>
                        <h3><a href="http://<%=l.getUrl()%>" target="_blank"><%=l.getName()%></a></h3>
                            <%} else {%>
                        <h3><a href="<%=l.getUrl()%>" target="_blank"><%=l.getName()%></a></h3>
                            <%}%>
                        <h4><%=l.getDescription()%></h4>
                        <div class="post-footer">
                            <small><%=l.getPoster()%>
                                <br />
                                <%=l.getTime().substring(0, l.getTime().lastIndexOf('.'))%>, <%=l.getDate()%></small></div>
                                <%if (l.getPoster().equals(username) || DBAccess.validAdmin(username)) {%>
                        <div>
                            <form action="DeleteLink" method="POST">
                                <input class="form-inline" type="hidden" value="<%=l.getId()%>" name="id">
                                <input type="submit" value="delete link" class="btn btn-danger">
                            </form>
                        </div>
                        <div >
                            <form class="form-inline" action="GoToEditLink" method="POST">
                                <input type="hidden" value="<%=l.getId()%>" name="id">  
                                <input type="submit" value="edit link" class="btn btn-success">
                            </form>
                        </div>
                        <%}%>
                    </div>
                    <%}
                        }%>
                </div>
                <div class="col-md-12"><p><small>copyright Macauley Scoffins 2015</small></p></div>
            </div>
        </div>
    </div>
</body>
</html>
