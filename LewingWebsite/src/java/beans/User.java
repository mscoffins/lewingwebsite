package beans;

import Controllers.Database.DBAccess;
import java.sql.SQLException;

public class User {
    private String username;
    private String name;
    private String email;
    private String img;
    private String password;

    public User(String username, String name, String email, String img) {
        this.username = username;
        this.name = name;
        this.email = email;
        this.img = img;
        this.password = "";
    }
    
    public User(String username, String name, String email, String img, String pw){
        this.username = username;
        this.name = name;
        this.email = email;
        this.img = img;
        this.password = pw;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }
    
    public String getPassword(){
        return this.password;
    }
    
    public boolean persist() throws ClassNotFoundException, SQLException{
        return DBAccess.persistUser(this);
    }
}
