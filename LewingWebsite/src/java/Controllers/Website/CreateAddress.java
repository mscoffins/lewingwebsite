/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers.Website;

import Controllers.Database.DBAccess;
import beans.Address;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Maco
 */
@WebServlet(name = "CreateAddress", urlPatterns = {"/CreateAddress"})
public class CreateAddress extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     * @throws java.lang.ClassNotFoundException
     * @throws java.sql.SQLException
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ClassNotFoundException, SQLException {
        response.setContentType("text/html;charset=UTF-8");
        try {
            String nameNum = request.getParameter("nameNum");
            String street = request.getParameter("street");
            String town = request.getParameter("town");
            String city = request.getParameter("city");
            String postcode = request.getParameter("postcode");
            String username = request.getParameter("username");

            Address a = new Address(0, nameNum, street, town, city, postcode);
            int id = a.persist();
            if (id == 0) {
                request.setAttribute("error", "unable to create new addess");
                request.getRequestDispatcher("AddressBook.jsp").forward(request, response);
            }
            boolean success = DBAccess.setAddress(username, id);
            if (success) {
                request.setAttribute("success", "address added");
                request.getRequestDispatcher("AddressBook.jsp").forward(request, response);
            } else {
                request.setAttribute("error", "unable to set address");
                request.getRequestDispatcher("AddressBook.jsp").forward(request, response);
            }
        } catch (ClassNotFoundException | SQLException | ServletException | IOException e) {
            System.out.println(e);
            request.setAttribute("error", "something went wrong");
            request.getRequestDispatcher("AddressBook.jsp").forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(CreateAddress.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(CreateAddress.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
