package Controllers.Database;

import beans.Address;
import beans.Gallery;
import beans.News;
import beans.NewsComment;
import beans.Picture;
import beans.UsefulLink;
import beans.User;
import beans.UserRequest;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class DBAccess {

    //method to establish a connection to the database
    public static Connection getConnection()
            throws ClassNotFoundException, SQLException {
        String jdbcDriver = "org.postgresql.Driver";
        Class.forName(jdbcDriver);
        String jdbcUrl = "jdbc:postgresql://localhost:5433/lewingdb";
        String username = "postgres";
        String password = "password";

        return (DriverManager.getConnection(jdbcUrl, username, password));
    }

    public static boolean validLogin(String username, String password) throws ClassNotFoundException, SQLException {

        boolean validLogin = false;
        try (Connection con = getConnection()) {
            PreparedStatement sql = con.prepareStatement("SELECT username FROM users WHERE (username = ? AND password = crypt(?,password))");
            sql.setString(1, username);
            sql.setString(2, password);
            ResultSet results = sql.executeQuery();
            if (results.next()) {
                validLogin = true;
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return validLogin;
    }

    public static boolean validUser(String username) throws SQLException, ClassNotFoundException {
        if (username.equalsIgnoreCase("guest")) {
            return false;
        }
        boolean validLogin = false;
        try (Connection con = getConnection()) {
            PreparedStatement sql = con.prepareStatement("SELECT username FROM users WHERE username = ?");
            sql.setString(1, username);
            ResultSet results = sql.executeQuery();
            if (results.next()) {
                validLogin = true;
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return validLogin;
    }

    public static User getUserByUsername(String externUsername) throws ClassNotFoundException, SQLException {

        User user = null;
        try (Connection con = DBAccess.getConnection()) {
            PreparedStatement sql = con.prepareStatement("SELECT * FROM users WHERE username = ?");
            sql.setString(1, externUsername);
            ResultSet results = sql.executeQuery();
            String name = null;
            String username = null;
            String email = null;
            String imageUrl = null;
            while (results.next()) {
                name = results.getString("name");
                username = results.getString("username");
                email = results.getString("email");
                imageUrl = results.getString("profileimg");
            }
            user = new User(name, username, email, imageUrl);
        } catch (Exception e) {
            System.out.println(e);
        }
        return user;
    }

    public static ArrayList<News> getNews() throws ClassNotFoundException, SQLException {

        ArrayList<News> news = null;
        try (Connection con = DBAccess.getConnection()) {
            PreparedStatement sql = con.prepareStatement("SELECT * FROM News ORDER BY date DESC,time DESC");
            ResultSet results = sql.executeQuery();
            news = new ArrayList<>();
            News newsItem;
            int id;
            String title;
            String content;
            String poster;
            String date;
            String time;
            boolean isPrivate;
            String image;
            while (results.next()) {
                id = results.getInt("id");
                title = results.getString("title");
                content = results.getString("content");
                poster = results.getString("poster");
                date = results.getString("date");
                time = results.getString("time");
                isPrivate = results.getBoolean("private");
                image = results.getString("image");
                newsItem = new News(id, title, content, poster, date, time, isPrivate, image);
                news.add(newsItem);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return news;
    }

    public static ArrayList<UsefulLink> getUsefulLinks() throws ClassNotFoundException, SQLException {

        ArrayList<UsefulLink> links = null;
        try (Connection con = DBAccess.getConnection()) {
            PreparedStatement sql = con.prepareStatement("SELECT * FROM UsefulLinks ORDER BY date DESC,time DESC");
            ResultSet results = sql.executeQuery();
            links = new ArrayList<>();
            UsefulLink link;
            int id;
            String name;
            String description;
            String url;
            String poster;
            String date;
            String time;
            boolean isPrivate;
            while (results.next()) {
                id = results.getInt("id");
                name = results.getString("name");
                description = results.getString("description");
                url = results.getString("url");
                poster = results.getString("poster");
                date = results.getString("date");
                time = results.getString("time");
                isPrivate = results.getBoolean("private");
                link = new UsefulLink(id, name, description, url, poster, date, time, isPrivate);
                links.add(link);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return links;
    }

    public static ArrayList<Address> getAddresses() throws ClassNotFoundException, SQLException {

        ArrayList<Address> addresses = null;
        try (Connection con = DBAccess.getConnection()) {
            PreparedStatement sql = con.prepareStatement("SELECT * FROM Address");
            ResultSet results = sql.executeQuery();
            addresses = new ArrayList<>();
            Address address;
            int id;
            String namenum;
            String street;
            String town;
            String city;
            String postcode;
            while (results.next()) {
                id = results.getInt("id");
                namenum = results.getString("namenum");
                street = results.getString("street");
                town = results.getString("town");
                city = results.getString("city");
                postcode = results.getString("postcode");
                address = new Address(id, namenum, street, town, city, postcode);
                addresses.add(address);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return addresses;
    }

    public static boolean persistNewsPost(News n) throws ClassNotFoundException, SQLException {

        try (Connection con = getConnection()) {
            PreparedStatement sql = con.prepareStatement("INSERT INTO News (title,content,poster,date,time,private,image) VALUES (?,?,?,CURRENT_DATE,CURRENT_TIME,?,?)");
            sql.setString(1, n.getTitle());
            sql.setString(2, n.getContent());
            sql.setString(3, n.getPoster());
            sql.setBoolean(4, n.isIsPrivate());
            sql.setString(5, n.getImage());

            sql.executeUpdate();
        } catch (ClassNotFoundException | SQLException e) {
            System.out.println(e);
            return false;
        }
        return true;
    }

    public static boolean persistUsefulLinkPost(UsefulLink l) throws ClassNotFoundException, SQLException {

        try (Connection con = getConnection()) {
            PreparedStatement sql = con.prepareStatement("INSERT INTO UsefulLinks (name,description,url,poster,date,time,private) VALUES (?,?,?,?,CURRENT_DATE,CURRENT_TIME,?)");
            sql.setString(1, l.getName());
            sql.setString(2, l.getDescription());
            sql.setString(3, l.getUrl());
            sql.setString(4, l.getPoster());
            sql.setBoolean(5, l.isIsPrivate());

            sql.executeUpdate();
        } catch (ClassNotFoundException | SQLException e) {
            System.out.println(e);
            return false;
        }
        return true;
    }

    public static boolean persistImage(Picture img) throws ClassNotFoundException, SQLException {
        try (Connection con = getConnection()) {
            PreparedStatement sql = con.prepareStatement("INSERT INTO Photos (description,loc,galleryid) VALUES (?,?,?)");

            sql.setString(1, img.getDecription());
            sql.setString(2, img.getLoc());
            sql.setInt(3, img.getGalleryID());

            sql.executeUpdate();
        } catch (Exception e) {
            System.out.println(e);
            return false;
        }
        return true;
    }

    public static ArrayList<Gallery> getGalleries() throws SQLException, ClassNotFoundException {
        ArrayList<Gallery> galleries = null;
        try (Connection con = getConnection()) {
            PreparedStatement sql = con.prepareStatement("SELECT * FROM Galleries");
            galleries = new ArrayList<>();
            int id;
            String name;
            String desc;
            String thumbnail;
            boolean isPrivate;
            String poster;
            ResultSet rs = sql.executeQuery();
            while (rs.next()) {
                id = rs.getInt("id");
                name = rs.getString("name");
                desc = rs.getString("description");
                thumbnail = rs.getString("thumbnail");
                isPrivate = rs.getBoolean("private");
                poster = rs.getString("poster");
                Gallery g = new Gallery(id, name, desc, thumbnail, isPrivate, poster);
                galleries.add(g);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return galleries;

    }

    public static ArrayList<Picture> getGalleryPics(int galleryID) throws ClassNotFoundException, SQLException {
        ArrayList<Picture> pics = null;
        try (Connection con = getConnection()) {
            PreparedStatement sql = con.prepareStatement("SELECT * FROM Photos WHERE galleryid = ?");
            sql.setInt(1, galleryID);
            pics = new ArrayList<>();
            int id;
            String loc;
            String desc;
            ResultSet rs = sql.executeQuery();
            while (rs.next()) {
                id = rs.getInt("id");
                loc = rs.getString("loc");
                desc = rs.getString("description");
                Picture g = new Picture(id, loc, desc, galleryID);
                pics.add(g);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return pics;
    }

    public static boolean persistGallery(Gallery g) throws ClassNotFoundException, SQLException {
        try (Connection con = getConnection()) {
            PreparedStatement sql = con.prepareStatement("INSERT INTO Galleries (name,description,thumbnail,private,poster) VALUES (?,?,?,?,?)");

            sql.setString(1, g.getName());
            sql.setString(2, g.getDescription());
            sql.setString(3, g.getThumbNail());
            sql.setBoolean(4, g.getIsPrivate());
            sql.setString(5, g.getPoster());

            sql.executeUpdate();
        } catch (Exception e) {
            System.out.println(e);
            return false;
        }
        return true;
    }

    public static ArrayList<String> getUsersAtAddress(int addressID) throws ClassNotFoundException, SQLException {
        ArrayList<String> names = null;
        try (Connection con = getConnection()) {
            PreparedStatement sql = con.prepareStatement("SELECT username FROM UserAddress WHERE AddressID = ?");
            sql.setInt(1, addressID);
            names = new ArrayList<>();
            ResultSet rs = sql.executeQuery();
            while (rs.next()) {
                String username = rs.getString("username");
                PreparedStatement sql2 = con.prepareStatement("SELECT name FROM Users WHERE username = ?");
                sql2.setString(1, username);
                ResultSet rs2 = sql2.executeQuery();
                if (rs2.next()) {
                    String name = rs2.getString("name");
                    names.add(name);
                }
            }
        }catch(Exception e){
            System.out.println(e);
        }
        return names;

    }

    public static int persistAddress(Address a) throws ClassNotFoundException, SQLException {
        int id = 0;
        try (Connection con = getConnection()) {
            PreparedStatement sql = con.prepareStatement("INSERT INTO Address (namenum,street,town,city,postcode) VALUES (?,?,?,?,?)");
            sql.setString(1, a.getNameNum());
            sql.setString(2, a.getStreet());
            sql.setString(3, a.getTown());
            sql.setString(4, a.getCity());
            sql.setString(5, a.getPostcode());
            sql.executeUpdate();
            PreparedStatement sql2 = con.prepareStatement("SELECT id FROM Address WHERE (namenum = ? AND street = ? AND town = ? AND city = ? AND postcode = ?)");
            sql2.setString(1, a.getNameNum());
            sql2.setString(2, a.getStreet());
            sql2.setString(3, a.getTown());
            sql2.setString(4, a.getCity());
            sql2.setString(5, a.getPostcode());
            ResultSet rs = sql2.executeQuery();
            id = 0;
            if (rs.next()) {
                id = rs.getInt("id");
            }
        }catch(Exception e){
            System.out.println(e);
        }
        return id;
    }

    public static boolean setAddress(String username, int id) throws ClassNotFoundException, SQLException {
        try (Connection con = getConnection()) {
            PreparedStatement sql = con.prepareStatement("DELETE FROM UserAddress WHERE username = ?");
            
            sql.setString(1, username);
            
            sql.executeUpdate();
            
            PreparedStatement sql2 = con.prepareStatement("INSERT INTO UserAddress (username,addressid) VALUES (?,?)");
            
            sql2.setString(1, username);
            sql2.setInt(2, id);
            
            sql2.executeUpdate();
        }catch(Exception e){
            System.out.println(e);
            return false;
        }
        return true;
    }

    public static boolean deleteNews(int id) throws ClassNotFoundException, SQLException {
        try (Connection con = getConnection()) {
            PreparedStatement sql = con.prepareStatement("DELETE FROM News WHERE id = ?");
            
            sql.setInt(1, id);
            
            sql.executeUpdate();
        }catch( Exception e){
            System.out.println(e);
            return false;
        }
        return true;
    }

    public static News getNewsByID(int id) throws ClassNotFoundException, SQLException {
        News n = null;
        try (Connection con = getConnection()) {
            PreparedStatement sql = con.prepareStatement("SELECT * FROM News WHERE id = ?");
            sql.setInt(1, id);
            n = null;
            String title;
            String content;
            String poster;
            String date;
            String time;
            boolean isPrivate;
            String image;
            ResultSet rs = sql.executeQuery();
            if (rs.next()) {
                title = rs.getString("title");
                content = rs.getString("content");
                poster = rs.getString("poster");
                date = rs.getString("date");
                time = rs.getString("time");
                isPrivate = rs.getBoolean("private");
                image = rs.getString("image");
                n = new News(id, title, content, poster, date, time, isPrivate, image);
            }
        }catch(Exception e){
            System.out.println(e);
        }
        return n;
    }

    public static boolean editNews(int id, String title, String content, boolean isPrivate, String img) throws ClassNotFoundException, SQLException {
        try (Connection con = getConnection()) {
            PreparedStatement sql = con.prepareStatement("UPDATE News SET(title,content,private,image) = (?,?,?,?) WHERE id = ?");
            
            sql.setString(1, title);
            sql.setString(2, content);
            sql.setBoolean(3, isPrivate);
            sql.setString(4, img);
            sql.setInt(5, id);
            
            sql.executeUpdate();
        }catch(Exception e){
            System.out.println(e);
            return false;
        }
        return true;
    }

    public static boolean deleteLink(int id) throws ClassNotFoundException, SQLException {
        try (Connection con = getConnection()) {
            PreparedStatement sql = con.prepareStatement("DELETE FROM usefulLinks WHERE id = ?");
            
            sql.setInt(1, id);
            
            sql.executeUpdate();
        }catch(Exception e){
            System.out.println(e);
            return false;
        }
        return true;
    }

    public static UsefulLink getLinkByID(int id) throws ClassNotFoundException, SQLException {
        UsefulLink l = null;
        try (Connection con = getConnection()) {
            PreparedStatement sql = con.prepareStatement("SELECT * FROM usefullinks WHERE id = ?");
            sql.setInt(1, id);
            l = null;
            String name;
            String desc;
            String url;
            String poster;
            String date;
            String time;
            boolean isPrivate;
            ResultSet rs = sql.executeQuery();
            if (rs.next()) {
                name = rs.getString("name");
                desc = rs.getString("description");
                url = rs.getString("url");
                poster = rs.getString("poster");
                date = rs.getString("date");
                time = rs.getString("time");
                isPrivate = rs.getBoolean("private");
                l = new UsefulLink(id, name, desc, url, poster, date, time, isPrivate);
            }
        }catch(Exception e){
            System.out.println(e);
        }
        return l;
    }

    public static boolean editLink(int id, String name, String desc, String url, boolean isPrivate) throws ClassNotFoundException, SQLException {
        try (Connection con = getConnection()) {
            PreparedStatement sql = con.prepareStatement("UPDATE usefullinks SET(name,description,url,private) = (?,?,?,?) WHERE id = ?");
            
            sql.setString(1, name);
            sql.setString(2, desc);
            sql.setString(3, url);
            sql.setBoolean(4, isPrivate);
            sql.setInt(5, id);
            
            sql.executeUpdate();
        }catch(Exception e){
            System.out.println(e);
            return false;
        }
        return true;
    }

    public static int getNextPhotoID() throws ClassNotFoundException, SQLException {
        int id = 0;
        try (Connection con = getConnection()) {
            PreparedStatement sql = con.prepareStatement("SELECT nextval('photos_id_seq')");
            id = 0;
            ResultSet rs = sql.executeQuery();
            if (rs.next()) {
                id = rs.getInt(1);
                PreparedStatement sql2 = con.prepareStatement("SELECT setval('photos_id_seq',?)");
                sql2.setInt(1, id - 1);
                sql2.executeQuery();
            }
        }catch(Exception e){
            System.out.println(e);
        }
        return id;

    }

    public static ArrayList<Picture> getPictures() throws ClassNotFoundException, SQLException {
        ArrayList<Picture> pics = null;
        try (Connection con = getConnection()) {
            PreparedStatement sql = con.prepareStatement("SELECT * FROM photos");
            ResultSet rs = sql.executeQuery();
            pics = new ArrayList<>();
            while (rs.next()) {
                int id = rs.getInt("id");
                String desc = rs.getString("description");
                int gId = rs.getInt("galleryid");
                String loc = rs.getString("loc");
                pics.add(new Picture(id, loc, desc, gId));
                
            }
        }catch(Exception e){
            System.out.println(e);
        }
        return pics;
    }

    public static boolean setThumbnail(String thumbnail, int id) throws ClassNotFoundException, SQLException {
        try (Connection con = getConnection()) {
            PreparedStatement sql = con.prepareStatement("UPDATE galleries SET(thumbnail) = (?) WHERE id = ?");
            
            sql.setString(1, thumbnail);
            sql.setInt(2, id);
            
            sql.executeUpdate();
        }catch(Exception e){
            System.out.println(e);
            return false;
        }
        return true;
    }

    public static boolean deleteGallery(int id) throws ClassNotFoundException, SQLException {
        try (Connection con = getConnection()) {
            PreparedStatement sql = con.prepareStatement("DELETE FROM photos WHERE galleryid = ?");
            
            sql.setInt(1, id);
            
            sql.executeUpdate();
            
            PreparedStatement sql2 = con.prepareStatement("DELETE FROM galleries WHERE id = ?");
            
            sql2.setInt(1, id);
            
            sql2.executeUpdate();
        }catch(Exception e){
            System.out.println(e);
            return false;
        }
        return true;

    }

    public static Gallery getGalleryByID(int id) throws SQLException, ClassNotFoundException {

        Gallery g = null;
        try (Connection con = DBAccess.getConnection()) {
            PreparedStatement sql = con.prepareStatement("SELECT * FROM galleries WHERE id = ?");
            sql.setInt(1, id);
            ResultSet results = sql.executeQuery();
            g = null;
            News newsItem;
            String name;
            String desc;
            String poster;
            boolean isPrivate;
            String thumbnail;
            while (results.next()) {
                
                name = results.getString("name");
                desc = results.getString("description");
                poster = results.getString("poster");
                isPrivate = results.getBoolean("private");
                thumbnail = results.getString("thumbnail");
                g = new Gallery(id, name, desc, thumbnail, isPrivate, poster);
                
            }
        }catch(Exception e){
            System.out.println(e);
        }
        return g;

    }

    public static boolean deleteImage(int id) throws ClassNotFoundException, SQLException {
        try (Connection con = getConnection()) {
            PreparedStatement sql = con.prepareStatement("DELETE FROM photos WHERE id = ?");
            sql.setInt(1, id);
            
            sql.executeUpdate();
        }catch(Exception e){
            System.out.println(e);
            return false;
        }
        return true;

    }

    public static ArrayList<UserRequest> getUserRequests() throws SQLException, ClassNotFoundException {
        ArrayList<UserRequest> urs = null;
        try (Connection con = getConnection()) {
            PreparedStatement sql = con.prepareStatement("SELECT * FROM userrequests");
            ResultSet rs = sql.executeQuery();
            urs = new ArrayList<>();
            int id;
            String name;
            String email;
            while (rs.next()) {
                id = rs.getInt("id");
                name = rs.getString("name");
                email = rs.getString("email");
                UserRequest ur = new UserRequest(id, name, email);
                urs.add(ur);
            }
        }catch(Exception e){
            System.out.println(e);
        }
        return urs;
    }

    public static boolean persistUser(User user) throws ClassNotFoundException, SQLException {

        try (Connection con = getConnection()) {
            PreparedStatement sql = con.prepareStatement("INSERT INTO users VALUES (?,?,?,crypt(?,gen_salt('md5')),?,CURRENT_TIMESTAMP)");
            
            sql.setString(1, user.getName());
            sql.setString(2, user.getUsername());
            sql.setString(3, user.getEmail());
            sql.setString(4, user.getPassword());
            sql.setString(5, user.getImg());
            
            sql.executeUpdate();
        }catch(Exception e){
            System.out.println(e);
        }

        return true;
    }

    public static boolean PersistUserRequest(UserRequest ur) throws ClassNotFoundException, SQLException {
        try (Connection con = getConnection()) {
            PreparedStatement sql = con.prepareStatement("INSERT INTO userrequests (name,email) VALUES (?,?)");
            
            sql.setString(1, ur.getName());
            sql.setString(2, ur.getEmail());
            
            sql.executeUpdate();
        }catch(Exception e){
            System.out.println(e);
            return false;
        }
        return true;

    }

    public static boolean changePassword(String username, String newpw) throws ClassNotFoundException, SQLException {
        try (Connection con = getConnection()) {
            PreparedStatement sql = con.prepareStatement("UPDATE users SET (password,pwexpire) = (crypt(?,gen_salt('md5')),CURRENT_TIMESTAMP + INTERVAL '1 year') WHERE username = ?");
            
            sql.setString(1, newpw);
            sql.setString(2, username);
            
            sql.executeUpdate();
        }catch(Exception e){
            System.out.println(e);
            return false;
        }
        return true;
    }

    public static boolean passwordExpired(String username) throws ClassNotFoundException, SQLException {
        try(Connection con = getConnection()){

            PreparedStatement sql = con.prepareStatement("SELECT * FROM users WHERE pwexpire < CURRENT_TIMESTAMP AND username = ?");

            sql.setString(1, username);

            ResultSet rs = sql.executeQuery();
            return rs.next();
        }catch(Exception e){
            System.out.println(e);
            return false;
        }

    }

    public static boolean validAdmin(String username) throws ClassNotFoundException, SQLException {
        try (Connection con = getConnection()) {
            PreparedStatement sql = con.prepareStatement("SELECT username FROM admins WHERE username = ?");
            sql.setString(1, username);
            ResultSet results = sql.executeQuery();
            if (results.next()) {
                return true;
            }
        }catch(Exception e){
            System.out.println(e);  
        }
        return false;
    }

    public static boolean removeRequest(int id) throws ClassNotFoundException, SQLException {
        try (Connection con = getConnection()) {
            PreparedStatement sql = con.prepareStatement("DELETE FROM userrequests WHERE id = ?");
            sql.setInt(1, id);
            
            sql.executeUpdate();
        }catch(Exception e){
            System.out.println(e);
            return false;
        }
        return true;
    }

    public static ArrayList<User> getUsers() throws ClassNotFoundException, SQLException {
        ArrayList<User> users = null;
        try (Connection con = getConnection()) {
            PreparedStatement sql = con.prepareStatement("SELECT * FROM users");
            ResultSet rs = sql.executeQuery();
            users = new ArrayList<>();
            String username;
            String name;
            String email;
            String img;
            while (rs.next()) {
                username = rs.getString("username");
                name = rs.getString("name");
                email = rs.getString("email");
                img = rs.getString("profileimg");
                User u = new User(username, name, email, img);
                users.add(u);
            }
        }catch(Exception e){
            System.out.println(e);
        }
        return users;
    }

    public static boolean expirePassword(String username) throws ClassNotFoundException, SQLException {
        try (Connection con = getConnection()) {
            PreparedStatement sql = con.prepareStatement("UPDATE users SET (pwexpire) = (CURRENT_TIMESTAMP) WHERE username = ?");
            
            sql.setString(1, username);
            
            sql.executeUpdate();
        }catch(Exception e){
            System.out.println(e);
            return false;
        }
        return true;
    }

    public static boolean removeUser(String username) throws ClassNotFoundException, SQLException, IOException {
        try (Connection con = getConnection()) {
            String SAVE_DIR = "Users/Maco/Documents/NetBeansProjects/LewingWebsite/web/Pics";
            String filePath = "C:" + File.separator + SAVE_DIR;
            PreparedStatement sql1 = con.prepareStatement("SELECT * FROM galleries WHERE poster = ?");
            sql1.setString(1, username);
            
            ResultSet rs = sql1.executeQuery();
            while (rs.next()) {
                PreparedStatement sql2 = con.prepareStatement("SELECT * FROM photos WHERE galleryid = ?");
                sql2.setInt(1, rs.getInt("id"));
                
                ResultSet rs2 = sql2.executeQuery();
                
                while (rs2.next()) {
                    String loc = rs2.getString("loc");
                    File img = new File(filePath, loc);
                    Files.deleteIfExists(img.toPath());
                }
            }
            
            PreparedStatement sql = con.prepareStatement("DELETE FROM users WHERE username = ?");
            sql.setString(1, username);
            
            sql.executeUpdate();
        }catch(Exception e){
            System.out.println(e);
            return false;
        }
        return true;
    }

    public static boolean persistComment(NewsComment comment) throws ClassNotFoundException, SQLException {
        try (Connection con = getConnection()) {
            PreparedStatement sql = con.prepareStatement("INSERT INTO newscomment(poster,newsid,comment,date,time) VALUES (?,?,?,CURRENT_DATE,CURRENT_TIME)");
            sql.setString(1, comment.getPoster());
            sql.setInt(2,comment.getNewsId());
            sql.setString(3,comment.getComment());
            
            sql.executeUpdate();  
        }catch(Exception e){
            System.out.println(e);
            return false;
        }
        
        return true;
        
    }
    
    public static ArrayList<NewsComment> getNewsComments(int newsId) throws ClassNotFoundException, SQLException{
        ArrayList<NewsComment> comments;
        comments = new ArrayList<>();
        try (Connection con = getConnection()) {
            PreparedStatement sql = con.prepareStatement("SELECT * FROM newscomment WHERE newsid = ?");
            sql.setInt(1,newsId);
            comments = new ArrayList<>();
            int id;
            String poster;
            String comment;
            String date;
            String time;
            ResultSet rs = sql.executeQuery();
            while(rs.next()){
                id = rs.getInt("id");
                poster = rs.getString("poster");
                comment = rs.getString("comment");
                date = rs.getString("date");
                time = rs.getString("time");
                NewsComment c = new NewsComment(id,poster,newsId,comment,date,time);
                comments.add(c);
            }
        }catch(Exception e){
            System.out.println(e);
            
        }
        return comments;
    }

    public static boolean editComment(int id, String comment) throws ClassNotFoundException, SQLException {
        try (Connection con = getConnection()) {
            PreparedStatement sql = con.prepareStatement("UPDATE newscomment SET (comment) = (?) WHERE id = ?");
            sql.setString(1, comment);
            sql.setInt(2, id);
            
            sql.executeUpdate();
        }catch(Exception e){
            System.out.println(e);
            return false;
        }
       return true;
    }

    public static boolean deleteComment(int id) throws ClassNotFoundException, SQLException {
        try (Connection con = getConnection()) {
            PreparedStatement sql = con.prepareStatement("DELETE FROM newscomment WHERE id = ?");
            sql.setInt(1, id);
            
            sql.executeUpdate();
        }catch(Exception e){
            System.out.println(e);
            return false;
        }
       return true;
    }

}
